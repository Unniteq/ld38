extends Node2D

var dialogs = {
	"intro_guy0" : {
		"title" : "The chosen one",
		"text" : "Boy oh boy am I going on an amazing aventure !",
		"next" : "intro_guy1"
	},
	
	"intro_guy1" : {
		"title" : "The chosen one",
		"text" : "Let's do this !",
	},
	
	"hero_start0" : {
		"title" : "Random dude",
		"text" : "what",
		"next" : "hero_start1"
	},
	
	"hero_start1" : {
		"title" : "Random dude",
		"text" : "What is happening ? Where am I ? And why ?",
		"next" : "hero_start2"
	},
	
	"hero_start2" : {
		"title" : "Random dude",
		"text" : "Also, why am I talking alone ? And why do I want to say that using ZQD/AWD or arrows makes me move and jump ?",
	},
	
	"hero_interaction0_0" : {
		"title" : "Random dude",
		"text" : "Weird, this looks like a castle, but it's so tiny !",
		"next" : "hero_interaction0_1"
	},
	
	"hero_interaction0_1" : {
		"title" : "Random dude",
		"text" : "I should be able to jump over it...",
	},
	
	"hero_interaction1_0" : {
		"title" : "Random dude",
		"text" : "OH FUCK",
		"next" : "hero_interaction1_1"
	},
	
	"hero_interaction1_1" : {
		"title" : "Random dude",
		"text" : "I did NOT mean to do that",
	},
	
	"hero_interaction2_0" : {
		"title" : "Random dude",
		"text" : "You could think that breaking hundreds of stones with my bare body may be painful.",
		"next" : "hero_interaction2_1"
	},
	
	"hero_interaction2_1" : {
		"title" : "Random dude",
		"text" : "And you would be right.",
		"next" : "hero_interaction2_2"
	},
	
	"hero_interaction2_2" : {
		"title" : "Random dude",
		"text" : "Help me.",
	},
	
	"hero_interaction3_0" : {
		"title" : "Random dude",
		"text" : "Oh well...",
	},
	
	"hero_interaction4_0" : {
		"title" : "Random dude",
		"text" : "Okay, I need to figure out what is happening, how I got here and why everything is so small.",
		"next" : "hero_interaction4_1"
	},
	
	"hero_interaction4_1" : {
		"title" : "Random dude",
		"text" : "I can't seem to help myself breaking almost everything I touched, although everything is pretty fragile.",
		"next" : "hero_interaction4_2"
	},
	
	"hero_interaction4_2" : {
		"title" : "Random dude",
		"text" : "Thank god I did not meet anyone (Well, I hope i did not). If these structures are so small, I guess the people that live here are too.",
		"next" : "hero_interaction4_3"
	},
	
	"hero_interaction4_2" : {
		"title" : "THE Chosen One",
		"text" : "It's decided then, I shall go on a journey to find the answers I seek. I will not rest unt-",
	},
	
	"end" : {
		"title" : "?????????",
		"text" : "what",
	},
	
}